package com.epam.maven_project;

import java.util.Scanner;

/**
 * Is a program that takes an interval of numbers from you, takes odd and not even numbers from that interval,
 * and forms a Fibonacci sequence from them.
 *
 * @author Nazar Horbal
 * @version 1.0
 * @since 2019-11-07
 */

public class Fibonacci {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int start_of_interval;                 // declaring all used variables
        int end_of_interval;
        int size_of_set;
        int sum_of_odd_numbers = 0;
        int sum_of_even_numbers = 0;
        int max_odd_number = 0;
        int max_even_number = 0;
        int numberOfOddFibonacciNumbers = -1;
        int numberOfEvenFibonacciNumbers = 2;
        float percentage_of_oddFibonacciNumbers;
        float percentage_of_evenFibonacciNumbers;
        System.out.println("Hello dear User!");
        System.out.println("Please enter the interval of numbers below(with space): ");
        start_of_interval = in.nextInt();
        end_of_interval = in.nextInt();
        System.out.print("Its all odd numbers in your interval: ");
        for (int i = start_of_interval; i <= end_of_interval; i++) {    // loop to search for odd numbers their sums
            if (i % 2 != 0) {
                System.out.print(i + " ");
                sum_of_odd_numbers += i;
                max_odd_number++;
                if (i > max_odd_number) {
                    max_odd_number = i;
                }
            }
        }
        System.out.println(">>> sum = " + sum_of_odd_numbers + ", max odd number = " + max_odd_number + ";");
        System.out.print("Its all even numbers in your interval: ");
        for (int i = start_of_interval; i <= end_of_interval; i++) {    // loop to search for even numbers their sums
            if (i % 2 == 0) {
                System.out.print(i + " ");
                sum_of_even_numbers += i;
                max_even_number++;
                if (i > max_even_number) {
                    max_even_number = i;
                }
            }
        }
        // output of numbers from loops
        System.out.println(">>> sum = " + sum_of_even_numbers + ", max even number = " + max_even_number + ";");
        System.out.println("Sum of odd and even numbers = " + (sum_of_odd_numbers + sum_of_even_numbers) + ";");
        System.out.println("Now we try to build Fibonacci numbers, please enter the size of set(etc: 7): ");
        size_of_set = in.nextInt();
        int F1 = max_odd_number;        // declaring variables for forming a number of Fibonacci
        int F2 = max_even_number;
        System.out.print("Numbers of Fibonacci: " + F1 + " ");
        for (int i = 1; i < size_of_set; i++) {  // loop for forming a number of Fibonacci
            System.out.print(F2 + " ");
            int nextNumber = F1 + F2;
            F1 = F2;
            F2 = nextNumber;
            if (F2 % 2 == 0) {
                numberOfEvenFibonacciNumbers++;
            } else {
                numberOfOddFibonacciNumbers++;
            }
        }
        System.out.print(";");
        percentage_of_oddFibonacciNumbers = (float) (numberOfOddFibonacciNumbers * 100) / (float) size_of_set;
        percentage_of_evenFibonacciNumbers = (float) (numberOfEvenFibonacciNumbers * 100) / (float) size_of_set;
        // Print percentage of odd and even Fibonacci numbers
        System.out.printf("\nPercentage of odd Fibonacci numbers: %.2f %%", percentage_of_oddFibonacciNumbers);
        System.out.printf("\nPercentage of even Fibonacci numbers: %.2f %%", percentage_of_evenFibonacciNumbers);
    }
}
